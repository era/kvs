use crate::kv::KvsEngine;
use crate::{error::Error, Result};
use sled::{Db, Tree};

// Mostly from: https://github.com/pingcap/talent-plan/blob/master/courses/rust/projects/project-3/src/engines/sled.rs
/// Wrapper of `sled::Db`
#[derive(Clone)]
pub struct SledStore(Db);

impl SledStore {
    pub fn new(db: Db) -> Self {
        Self(db)
    }
}

impl KvsEngine for SledStore {
    fn set(&self, key: String, value: String) -> Result<()> {
        let tree: &Tree = &self.0;
        tree.insert(key, value.into_bytes()).map(|_| ()).unwrap();
        tree.flush().unwrap();
        Ok(())
    }

    fn get(&self, key: String) -> Result<Option<String>> {
        let tree: &Tree = &self.0;
        Ok(tree
            .get(key)
            .unwrap()
            .map(|i_vec| AsRef::<[u8]>::as_ref(&i_vec).to_vec())
            .map(String::from_utf8)
            .transpose()
            .unwrap())
    }

    fn remove(&self, key: String) -> Result<()> {
        let tree: &Tree = &self.0;
        let result = tree.remove(key).unwrap();

        if result.is_none() {
            return Err(Error::NonExistingKey);
        }

        tree.flush().unwrap();
        Ok(())
    }
}

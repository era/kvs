use crate::error::{Error, Result};
use crate::thread_pool::shared_queue::SharedQueue;
use crate::thread_pool::ThreadPool;
use serde::{Deserialize, Serialize};
use serde_json::Deserializer;
use std::cell::RefCell;
use std::collections::HashMap;
use std::ffi::OsStr;
use std::fs::{self, File, OpenOptions};
use std::io::{self, BufReader, BufWriter, Read, Seek, SeekFrom, Write};
use std::ops::Range;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::sync::Mutex;
use std::sync::RwLock;

const MAX_UNCOMPACTED_SIZE: u64 = 1024 * 1024;

#[derive(Clone)]
pub struct KvStore {
    reader: Arc<RwLock<KvStoreReader>>,
    writer: Arc<Mutex<KvStoreWriter>>,
    index: Arc<RwLock<HashMap<String, CommandPos>>>,
    wal_folder: PathBuf,
    thread_pool: SharedQueue,
    pub compacting: Arc<Mutex<i8>>,
}

pub struct KvStoreReader {
    readers: HashMap<u64, BufReaderWithPos<File>>,
}

pub struct KvStoreWriter {
    index: Arc<RwLock<HashMap<String, CommandPos>>>,
    writer: BufWriterWithPos<File>,
    current_gen: u64,
    uncompacted: u64,
}

impl Drop for KvStore {
    fn drop(&mut self) {
        //TODO improve
        // workaround to wait the compacting to be done before dropping
        // this is needed because compacting is done in another thread
        // and may corrupt the file
        let mut compacting = self.compacting.lock().unwrap(); // TODO
        *compacting = 1
    }
}

impl super::KvsEngine for KvStore {
    fn set(&self, key: String, value: String) -> Result<()> {
        let uncompacted = self.writer.lock().unwrap().set(key, value)?;

        if uncompacted > MAX_UNCOMPACTED_SIZE {
            let cloned = self.clone();
            self.thread_pool.spawn(move || {
                let mut compacting = cloned.compacting.lock().unwrap();
                if *compacting == 1 {
                    //TODO improve this workaround, check Drop for details
                    return;
                }

                *compacting = 1;
                cloned.compact().expect("could not compact");
                *compacting = 0;
            });
        }

        Ok(())
    }

    fn get(&self, key: String) -> Result<Option<String>> {
        let index = self.index.read().unwrap();
        match index.get(&key) {
            Some(cmd_pos) => self.reader.write().unwrap().get(cmd_pos),
            None => Ok(None),
        }
    }

    fn remove(&self, key: String) -> Result<()> {
        self.writer.lock().unwrap().remove(key)
    }
}

impl KvStoreWriter {
    pub fn remove(&mut self, key: String) -> Result<()> {
        let mut index = self.index.write().unwrap();
        match index.remove(&key) {
            Some(_) => {
                let command = Command::Remove { key };
                serde_json::to_writer(&mut self.writer, &command)
                    .map_err(|e| Error::IOError(e.to_string()))?;

                self.writer
                    .flush()
                    .map_err(|e| Error::IOError(e.to_string()))?;

                Ok(())
            }
            _ => Err(Error::NonExistingKey),
        }
    }

    pub fn set(&mut self, key: String, value: String) -> Result<u64> {
        let mut index = self.index.write().unwrap();
        let command = Command::Set { key, value };
        let pos = self.writer.pos;

        serde_json::to_writer(&mut self.writer, &command).unwrap();

        self.writer
            .flush()
            .map_err(|e| Error::IOError(e.to_string()))
            .unwrap();

        if let Command::Set { key, .. } = command {
            if let Some(old) = index.insert(key, (self.current_gen, pos..self.writer.pos).into()) {
                self.uncompacted += old.len;
            }
        }

        Ok(self.uncompacted)
    }
}

impl KvStoreReader {
    pub fn get(&mut self, cmd_pos: &CommandPos) -> Result<Option<String>> {
        let reader = self
            .readers
            .get_mut(&cmd_pos.gen)
            .expect("Cannot find log reader");

        reader
            .seek(SeekFrom::Start(cmd_pos.pos))
            .map_err(|e| Error::IOError(e.to_string()))?;

        let cmd_reader = reader.take(cmd_pos.len);
        if let Command::Set { value, .. } =
            serde_json::from_reader(cmd_reader).map_err(|e| Error::IOError(e.to_string()))?
        {
            Ok(Some(value))
        } else {
            Err(Error::UnexpectedCommandType)
        }
    }
    pub fn delete_stale(&mut self, compaction_gen: u64, wal_folder: &Path) -> Result<()> {
        // remove stale log files.
        let stale_gens: Vec<_> = self
            .readers
            .keys()
            .filter(|&&gen| gen < compaction_gen)
            .cloned()
            .collect();

        for stale_gen in stale_gens {
            self.readers.remove(&stale_gen);
            fs::remove_file(log_path(wal_folder, stale_gen))
                .map_err(|e| Error::IOError(e.to_string()))?;
        }
        Ok(())
    }
}

impl KvStore {
    pub fn open(path: impl Into<PathBuf>) -> Result<Self> {
        let path = path.into();
        std::fs::create_dir_all(&path).map_err(|e| Error::IOError(e.to_string()))?;

        let mut readers = HashMap::new();

        let mut index = HashMap::new();
        let mut uncompacted: u64 = 0;

        let gens = sorted_gen_list(&path)?;

        let last_gen = 0;

        for gen in gens {
            let mut reader = BufReaderWithPos::new(
                File::open(log_path(&path, gen)).map_err(|e| Error::IOError(e.to_string()))?,
            )
            .unwrap();

            uncompacted += load(gen, &mut reader, &mut index)?;

            readers.insert(gen, reader);
        }

        let writer = new_log_file(&path, last_gen, &mut readers)?;

        let index = Arc::new(RwLock::new(index));

        let kv_writer = KvStoreWriter {
            writer,
            current_gen: last_gen,
            uncompacted,
            index: index.clone(),
        };

        let kv_reader = KvStoreReader { readers };

        Ok(Self {
            wal_folder: path,
            reader: Arc::new(RwLock::new(kv_reader)),
            writer: Arc::new(Mutex::new(kv_writer)),
            index: index.clone(),
            thread_pool: SharedQueue::new(2).unwrap(),
            compacting: Arc::new(Mutex::new(0)),
        })
    }

    /// Create a new log file with given generation number and add the reader to the readers map.
    ///
    /// Returns the writer to the log.
    fn new_log_file(&self, gen: u64) -> Result<BufWriterWithPos<File>> {
        let mut reader = self.reader.write().unwrap();
        new_log_file(&self.wal_folder, gen, &mut reader.readers)
    }

    pub fn compact(&self) -> Result<()> {
        let compaction_gen = {
            let mut writer = self.writer.lock().unwrap();
            // increase current gen by 2. current_gen + 1 is for the compaction file.
            let compaction_gen = writer.current_gen + 1;
            writer.current_gen += 2;
            writer.writer = self.new_log_file(writer.current_gen)?;
            compaction_gen
        };

        let mut compaction_writer = self.new_log_file(compaction_gen)?;

        let mut current_pos = 0; // pos in the new log file.
        let mut snapshot = {
            let index = self.index.read().unwrap();
            index.clone()
        };
        for cmd_pos in &mut snapshot.values_mut() {
            if cmd_pos.gen > compaction_gen {
                continue;
            }
            //TODO avoid holding the lock since we don't really need it
            // besides to re-use the file descriptor
            let mut reader = self.reader.write().unwrap();
            let reader = reader
                .readers
                .get_mut(&cmd_pos.gen)
                .expect("Cannot find log reader");
            if reader.pos != cmd_pos.pos {
                reader
                    .seek(SeekFrom::Start(cmd_pos.pos))
                    .map_err(|e| Error::IOError(e.to_string()))?;
            }

            let mut entry_reader = reader.take(cmd_pos.len);

            let len = io::copy(&mut entry_reader, &mut compaction_writer)
                .map_err(|e| Error::IOError(e.to_string()))?;

            *cmd_pos = (compaction_gen, current_pos..current_pos + len).into();
            current_pos += len;
        }
        compaction_writer
            .flush()
            .map_err(|e| Error::IOError(e.to_string()))?;

        {
            let mut index = self.index.write().unwrap();

            for (key, value) in index.iter_mut() {
                if value.gen < compaction_gen {
                    *value = match snapshot.get(key) {
                        Some(e) => e.clone(),
                        _ => panic!("value should be in snapshot"),
                    };
                }
            }
        }

        {
            self.reader
                .write()
                .unwrap()
                .delete_stale(compaction_gen, &self.wal_folder)?;
        }

        self.writer.lock().unwrap().uncompacted = 0;

        Ok(())
    }
}

fn sorted_gen_list(path: &Path) -> Result<Vec<u64>> {
    let mut gen_list: Vec<u64> = fs::read_dir(path)
        .unwrap()
        .flat_map(|res| -> Result<_> { Ok(res.unwrap().path()) })
        .filter(|path| path.is_file() && path.extension() == Some("log".as_ref()))
        .flat_map(|path| {
            path.file_name()
                .and_then(OsStr::to_str)
                .map(|s| s.trim_end_matches(".log"))
                .map(str::parse::<u64>)
        })
        .flatten()
        .collect();
    gen_list.sort_unstable();
    Ok(gen_list)
}

fn new_log_file(
    path: &Path,
    gen: u64,
    readers: &mut HashMap<u64, BufReaderWithPos<File>>,
) -> Result<BufWriterWithPos<File>> {
    let path = log_path(path, gen);
    let writer = BufWriterWithPos::new(
        OpenOptions::new()
            .create(true)
            .write(true)
            .append(true)
            .open(&path)
            .unwrap(),
    )
    .unwrap();

    readers
        .entry(gen)
        .or_insert_with(|| BufReaderWithPos::new(File::open(&path).unwrap()).unwrap());

    Ok(writer)
}

/// Load the whole log file and store value locations in the index map.
///
/// Returns how many bytes can be saved after a compaction.
fn load(
    gen: u64,
    reader: &mut BufReaderWithPos<File>,
    index: &mut HashMap<String, CommandPos>,
) -> Result<u64> {
    // To make sure we read from the beginning of the file.
    let mut pos = reader
        .seek(SeekFrom::Start(0))
        .map_err(|e| Error::IOError(e.to_string()))?;

    let mut stream = Deserializer::from_reader(reader).into_iter();
    let mut uncompacted = 0; // number of bytes that can be saved after a compaction.
    while let Some(cmd) = stream.next() {
        let new_pos = stream.byte_offset() as u64;
        match cmd.unwrap() {
            //TODO
            Command::Set { key, .. } => {
                if let Some(old_cmd) = index.insert(key, (gen, pos..new_pos).into()) {
                    uncompacted += old_cmd.len;
                }
            }
            Command::Remove { key } => {
                if let Some(old_cmd) = index.remove(&key) {
                    uncompacted += old_cmd.len;
                }
                // the "remove" command itself can be deleted in the next compaction.
                // so we add its length to `uncompacted`.
                uncompacted += new_pos - pos;
            }
        }
        pos = new_pos;
    }
    Ok(uncompacted)
}

fn log_path(dir: &Path, gen: u64) -> PathBuf {
    dir.join(format!("{}.log", gen))
}

#[derive(Serialize, Deserialize, Debug)]
enum Command {
    Set { key: String, value: String },
    Remove { key: String },
}

/// Represents the position and length of a bson-serialized command in the log.
#[derive(Clone)]
struct CommandPos {
    gen: u64,
    pos: u64,
    len: u64,
}

impl From<(u64, Range<u64>)> for CommandPos {
    fn from((gen, range): (u64, Range<u64>)) -> Self {
        CommandPos {
            gen,
            pos: range.start,
            len: range.end - range.start,
        }
    }
}

struct BufReaderWithPos<R: Read + Seek> {
    reader: BufReader<R>,
    pos: u64,
}

impl<R: Read + Seek> BufReaderWithPos<R> {
    fn new(mut inner: R) -> Result<Self> {
        let pos = inner
            .seek(SeekFrom::Current(0))
            .map_err(|e| Error::IOError(e.to_string()))?;
        Ok(BufReaderWithPos {
            reader: BufReader::new(inner),
            pos,
        })
    }
}

impl<R: Read + Seek> Read for BufReaderWithPos<R> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let len = self.reader.read(buf)?;
        self.pos += len as u64;
        Ok(len)
    }
}

impl<R: Read + Seek> Seek for BufReaderWithPos<R> {
    fn seek(&mut self, pos: SeekFrom) -> io::Result<u64> {
        self.pos = self.reader.seek(pos)?;
        Ok(self.pos)
    }
}

struct BufWriterWithPos<W: Write + Seek> {
    writer: BufWriter<W>,
    pos: u64,
}

impl<W: Write + Seek> BufWriterWithPos<W> {
    fn new(mut inner: W) -> Result<Self> {
        let pos = inner
            .stream_len()
            .map_err(|e| Error::IOError(e.to_string()))?;
        Ok(BufWriterWithPos {
            writer: BufWriter::new(inner),
            pos,
        })
    }
}

impl<W: Write + Seek> Write for BufWriterWithPos<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let len = self.writer.write(buf)?;
        self.pos += len as u64;
        Ok(len)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.writer.flush()
    }
}

impl<W: Write + Seek> Seek for BufWriterWithPos<W> {
    fn seek(&mut self, pos: SeekFrom) -> io::Result<u64> {
        self.pos = self.writer.seek(pos)?;
        Ok(self.pos)
    }
}

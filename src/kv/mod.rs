use crate::Result;
pub use kv_engine::KvStore;
pub mod kv_engine;
pub mod sled_engine;

pub trait KvsEngine: Clone + Send + 'static {
    fn set(&self, key: String, value: String) -> Result<()>;

    fn get(&self, key: String) -> Result<Option<String>>;

    fn remove(&self, key: String) -> Result<()>;
}

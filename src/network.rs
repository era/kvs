use crate::error::{Error, Result};
use crate::kv::sled_engine::SledStore;
use crate::thread_pool::naive::NaiveThreadPool;
use crate::thread_pool::ThreadPool;
use crate::KvStore;
use crate::KvsEngine;
use serde::{Deserialize, Serialize};
use serde_json::{Deserializer, Serializer};
use std::env::current_dir;
use std::io::BufRead;
use std::io::{BufReader, BufWriter};
use std::path::PathBuf;
use std::sync::Arc;

use std::io::Read;
use std::io::Write;
use std::net::{TcpListener, TcpStream};

use std::net::SocketAddr;

use clap::{Parser, ValueEnum};

#[derive(Clone, Debug, ValueEnum)]
pub enum Engine {
    Kvs,
    Sled,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Command {
    Get { key: String },
    Set { key: String, value: String },
    Rm { key: String },
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Response {
    status: u16,
    body: core::result::Result<Option<String>, Error>,
}

#[derive(Clone)]
pub enum ServerStore {
    Kvs(Arc<KvStore>),
    Sled(Arc<SledStore>),
}

pub struct Server<T: ThreadPool> {
    store: ServerStore,
    thread_pool: T,
    pub listener: TcpListener,
}

impl<T: ThreadPool> Server<T> {
    pub fn new(addr: String, engine: Engine, thread_pool: T) -> Self {
        let curr_dir = current_dir().unwrap();
        Self::new_with_dir(addr, engine, thread_pool, curr_dir)
    }

    pub fn new_with_dir(
        addr: String,
        engine: Engine,
        thread_pool: T,
        mut curr_dir: PathBuf,
    ) -> Self {
        let store = match engine {
            Engine::Kvs => {
                let mut check_previous_version = curr_dir.clone();
                check_previous_version.push("sled");
                Server::<T>::panic_if_file_exists(check_previous_version);

                let store = Arc::new(KvStore::open(curr_dir.clone()).unwrap());
                curr_dir.push("kvs");
                std::fs::File::create(curr_dir).unwrap();
                ServerStore::Kvs(store)
            }
            Engine::Sled => {
                let mut check_previous_version = curr_dir.clone();
                check_previous_version.push("kvs");
                Server::<T>::panic_if_file_exists(check_previous_version);

                let store = Arc::new(SledStore::new(sled::open(curr_dir.clone()).unwrap()));

                curr_dir.push("sled");

                std::fs::File::create(curr_dir).unwrap();
                ServerStore::Sled(store)
            }
        };

        let listener = TcpListener::bind(&addr).unwrap();

        Self {
            store,
            thread_pool,
            listener,
        }
    }

    pub fn panic_if_file_exists(path: std::path::PathBuf) {
        if path.as_path().exists() {
            panic!("cannot start kvs on a folder which has sled content");
        }
    }

    pub fn listen(&mut self) -> Result<()> {
        for stream in self.listener.incoming() {
            let store = self.store.clone();
            self.thread_pool.spawn(move || {
                handle_client(
                    stream.map_err(|e| Error::TcpStream(e.to_string())).unwrap(),
                    store,
                )
            });
        }
        Ok(())
    }
    pub fn kill(&mut self) {}
}

fn handle_client(stream: TcpStream, mut store: ServerStore) {
    let reader = BufReader::new(&stream);
    let mut writer = BufWriter::new(&stream);
    let reqs = Deserializer::from_reader(reader).into_iter::<Command>();
    for req in reqs {
        match req {
            Ok(request) => {
                log::info!("received request {:?}", request);
                let response = process(request, &mut store);
                let result = respond_client(response);

                serde_json::to_writer(&mut writer, &result).unwrap();
                writer.flush().unwrap();
            }
            Err(e) => log::error!("{:?}", e),
        }
    }
}

fn respond_client(result: Result<Option<String>>) -> Response {
    match result {
        Err(e) => Response {
            status: 500,
            body: Err(e),
        },
        Ok(Some(s)) => Response {
            status: 200,
            body: Ok(Some(s)),
        },
        Ok(None) => Response {
            status: 200,
            body: Ok(None),
        },
    }
}

fn process(command: Command, store: &mut ServerStore) -> Result<Option<String>> {
    match command {
        Command::Get { key } => match &store {
            ServerStore::Kvs(db) => db.get(key),
            ServerStore::Sled(db) => db.get(key),
        },
        Command::Set { key, value } => {
            match &store {
                ServerStore::Kvs(db) => db.set(key, value)?,
                ServerStore::Sled(db) => db.set(key, value)?,
            }
            Ok(None)
        }
        Command::Rm { key } => {
            match &store {
                ServerStore::Kvs(db) => db.remove(key)?,
                ServerStore::Sled(db) => db.remove(key)?,
            }
            Ok(None)
        }
    }
}

#[derive(Clone)]
pub struct Client {
    addr: String,
}

impl Client {
    pub fn new(addr: String) -> Self {
        Self { addr }
    }

    // Send the command over the wire to the server
    // if the command is a get, returns the value or None
    // for all other commands it always return None
    pub fn send(&self, command: Command) -> Result<Option<String>> {
        let stream = TcpStream::connect(&self.addr).unwrap();
        let tcp_reader = stream.try_clone().unwrap();

        let mut reader = Deserializer::from_reader(BufReader::new(tcp_reader));
        let mut writer = BufWriter::new(stream);

        serde_json::to_writer(&mut writer, &command).unwrap();

        writer.flush().unwrap();

        let response = Response::deserialize(&mut reader).unwrap();

        response.body
    }
}

use crossbeam::channel::{self, Receiver, Sender};

use super::ThreadPool;
use crate::Result;

enum ThreadPoolMessage {
    RunJob(Box<dyn FnOnce() + Send + 'static>),
    Shutdown,
}

#[derive(Clone)]
pub struct SharedQueue {
    sender: Sender<Box<dyn FnOnce() + Send + 'static>>,
}

pub struct Worker {
    job_receiver: Receiver<Box<dyn FnOnce() + Send + 'static>>,
}

impl ThreadPool for SharedQueue {
    fn new(threads: u32) -> Result<Self> {
        let (tx, rx) = channel::unbounded::<Box<dyn FnOnce() + Send + 'static>>();
        for _ in 0..threads {
            let mut worker = Worker {
                job_receiver: rx.clone(),
            };
            std::thread::Builder::new()
                .spawn(move || worker.execute())
                .expect("could not create threads");
        }

        Ok(Self { sender: tx })
    }

    fn spawn<F>(&self, job: F)
    where
        F: FnOnce() + Send + 'static,
    {
        self.sender.send(Box::new(job));
    }
}

impl Drop for Worker {
    fn drop(&mut self) {
        if std::thread::panicking() {
            let mut worker = Worker {
                job_receiver: self.job_receiver.clone(),
            };
            if let Err(e) = std::thread::Builder::new().spawn(move || worker.execute()) {
                log::error!("Failed to spawn a thread: {}", e);
            }
        }
    }
}

impl Worker {
    fn execute(&mut self) {
        loop {
            match self.job_receiver.recv() {
                Ok(task) => {
                    task();
                }
                Err(e) => log::debug!("{:?}", e),
            };
        }
    }
}

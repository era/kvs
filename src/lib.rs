#![feature(seek_stream_len)]
pub use error::Error;
pub use error::Result;
pub use kv::sled_engine::SledStore;
pub use kv::KvStore;
pub use kv::KvsEngine;

mod error;
mod kv;
//TODO not really what we want here
pub mod helper;

pub mod network;
pub mod thread_pool;

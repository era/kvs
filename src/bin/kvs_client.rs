use clap::Parser;
use kvs::Error::NonExistingKey;

use kvs::helper;
use std::process::exit;

const DEFAULT_LISTENING_ADDRESS: &str = "127.0.0.1:4000";

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[command(subcommand)]
    action: Action,
}

#[derive(clap::Subcommand, Debug)]
enum Action {
    Set {
        key: String,
        value: String,
        #[arg(short, long)]
        addr: Option<String>,
    },
    Get {
        key: String,
        #[arg(short, long)]
        addr: Option<String>,
    },
    Rm {
        key: String,
        #[arg(short, long)]
        addr: Option<String>,
    },
}

fn main() {
    let args = Args::parse();

    match args.action {
        Action::Set { key, value, addr } => {
            let client =
                kvs::network::Client::new(addr.unwrap_or(DEFAULT_LISTENING_ADDRESS.to_string()));

            let command = kvs::network::Command::Set { key, value };
            client.send(command).unwrap();
        }
        Action::Get { key, addr } => {
            let client =
                kvs::network::Client::new(addr.unwrap_or(DEFAULT_LISTENING_ADDRESS.to_string()));

            let command = kvs::network::Command::Get { key };
            let get = client.send(command);

            helper::handle_get(get);
        }
        Action::Rm { key, addr } => {
            let client =
                kvs::network::Client::new(addr.unwrap_or(DEFAULT_LISTENING_ADDRESS.to_string()));

            let command = kvs::network::Command::Rm { key };
            match client.send(command) {
                Err(NonExistingKey) => {
                    eprintln!("Key not found");
                    exit(-1);
                }
                Err(e) => {
                    println!("{:?}", e);
                    exit(-1);
                }
                _ => {
                    // key removed with success
                    // the tests from the course expect the output
                    // to be empty
                    exit(0);
                }
            }
        }
    };
}

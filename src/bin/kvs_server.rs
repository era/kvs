use clap::{Parser};
use kvs::network::Engine;
use kvs::thread_pool::shared_queue::SharedQueue;
use kvs::thread_pool::ThreadPool;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    addr: Option<String>,
    #[arg(short, long)]
    engine: Option<Engine>,
}

fn main() {
    env_logger::init();

    let args = Args::parse();
    let addr = args.addr.unwrap_or("127.0.0.1:4000".to_string());
    let mut server = kvs::network::Server::new(
        addr,
        args.engine.unwrap_or(Engine::Kvs),
        SharedQueue::new(10).unwrap(),
    );
    server.listen().unwrap();
}

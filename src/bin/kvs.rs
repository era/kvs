//! kvs is a local only library
//! it will append operations into the current_dir
//! it's meant only as a way to test the library
//! since it's not possible to setup any configuration in it
//! you can do cargo run kvs --help
//! to get more information on how to run it

use clap::Parser;

use kvs::helper;
use kvs::KvsEngine;
use std::env::current_dir;
use std::process::exit;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[command(subcommand)]
    action: Action,
}

#[derive(clap::Subcommand, Debug)]
enum Action {
    Set { key: String, value: String },
    Get { key: String },
    Rm { key: String },
}

fn main() {
    let args = Args::parse();
    let store = kvs::KvStore::open(current_dir().unwrap());

    match args.action {
        Action::Set { key, value } => store.unwrap().set(key, value),

        Action::Get { key } => {
            let result = store.unwrap().get(key);
            helper::handle_get(result);
            Ok(())
        }
        Action::Rm { key } => {
            let result = store.unwrap().remove(key);
            match result {
                Ok(_) => result,
                Err(kvs::Error::NonExistingKey) => {
                    println!("Key not found");
                    exit(1);
                }
                Err(e) => {
                    println!("Error while trying to remove key {:?}", e);
                    exit(1);
                }
            }
        }
    }
    .unwrap();
}

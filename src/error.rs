use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub enum Error {
    IOError(String),
    SerializationError(String),
    UnexpectedCommandType,
    NonExistingKey,
    TcpStream(String),
    GenericError(String),
}

pub type Result<T> = std::result::Result<T, Error>;

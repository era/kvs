use std::process::exit;

pub fn handle_get(result: Result<Option<String>, crate::error::Error>) {
    match result {
        Ok(Some(ref result)) => println!("{result}"),
        Ok(None) => println!("Key not found"),
        Err(ref e) => {
            eprintln!("Error while getting result {:?}", e);
            exit(1);
        }
    };
}

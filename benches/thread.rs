use criterion::{criterion_group, criterion_main, BatchSize, Criterion};
use kvs::network::Client;
use kvs::network::Engine;
use kvs::network::Server;
use kvs::thread_pool::naive::NaiveThreadPool;
use kvs::thread_pool::shared_queue::SharedQueue;
use kvs::thread_pool::ThreadPool;
use kvs::{KvStore, KvsEngine, SledStore};
use rand::prelude::*;
use std::os::unix::io::AsRawFd;

use tempfile::TempDir;

fn heavy_write(c: &mut Criterion) {
    let mut group = c.benchmark_group("thread_write");
    let number_of_threads = &[2, 4, 8, 16, 32];

    for i in number_of_threads {
        group.bench_with_input(format!("thread_write_{i}"), i, |b, num| {
            let naive = NaiveThreadPool::new(1).unwrap();
            let num = (*num).clone();

            let temp_dir = TempDir::new().unwrap();

            let mut server = Server::new_with_dir(
                "127.0.0.1:0".to_string(),
                Engine::Kvs,
                SharedQueue::new(num).unwrap(),
                temp_dir.path().to_owned(),
            );
            let fd = server.listener.as_raw_fd();
            let addr = server.listener.local_addr().unwrap();
            naive.spawn(move || {
                server.listen().unwrap();
                println!("temp dir {:?}", temp_dir)
            });

            let client = Client::new(addr.to_string());
            let client_pool = SharedQueue::new(10).unwrap();

            b.iter(move || {
                for _ in 0..1000 {
                    let new_client = client.clone();
                    client_pool.spawn(move || {
                        new_client
                            .send(kvs::network::Command::Set {
                                key: "something".to_string(),
                                value: "something".to_string(),
                            })
                            .unwrap(); //TODO
                    });
                }
            });
            // We need a way to stop the tcpListener after the test
            // unsafe {
            //     libc::shutdown(fd, libc::SHUT_RD);
            // }
        });
    }
    group.finish();
}

criterion_group!(benches, heavy_write);
criterion_main!(benches);
